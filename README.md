# Outlook Signatures

**Best Resources**
1. https://www.msoutlook.info/question/backup-and-restore-signatures
2. https://www.msoutlook.info/question/signature-images-not-sending-or-visible-for-receiver 

## How It Works

I copied all the files and folders from `C:\Users\salter\AppData\Roaming\Microsoft\Signatures` into this folder, modified them, then copied them back over with my updates.

## Todo

* I think Prowess Test 1.htm is fixed, copied it over to the source folder too, but Litmus is down and I can't test it. Just need to go to https://litmus.com/previews and send an email to see for sure. Compare to signature-***.png's.
* Need to figure out how to change the images in the .rtf file too
* Need to make sure the .txt file doesn't have extra spaces like in Julian's email. Need to view email as plain text to see it?